import java.sql.*;
import java.util.ArrayList;


public class DataBase {
    private Connection con;
    private Statement st;
    private ResultSet rs;
    private PreparedStatement pst;

    public DataBase(){
        try{Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://sql8.freemysqlhosting.net/sql8175801","sql8175801","bI18P8H9Av"); //url / username / password
            //jei reiks priejimo prie db account sakykit
            st = con.createStatement();
        }
        catch (Exception ex){
            System.out.println("Error: "+ex);
        }
    }
    public void getData(){
        try{
            String query = "select * from parts";
            rs = st.executeQuery(query);
            rs = st.executeQuery(query);
            System.out.println("Records from Database");{
                while (rs.next()){
                /*String name=rs.getString("name"); //for testing purposes
                System.out.println("name : "+name);*/

                }
            }
        }

        catch (Exception ex){
            System.out.println("Error: "+ex);
        }
    }
    public void insert()throws Exception {

        final String var1 = "testPartName";
        final String var2 = "testTest";
        final String var3 = "testPartDescription";
        final int var4 = 20;
        try {
            pst = con.prepareStatement("INSERT INTO parts (name, test, description, results) VALUES ('"+var1+"','"+var2+"','"+var3+"','"+var4+"')");
            pst.executeUpdate();
        }catch (Exception ex){
            System.out.println("Exception : "+ex);
        }

    }
    public ArrayList<Part> getParts() throws Exception {
        try {
            pst = con.prepareStatement("SELECT * FROM parts");
            rs = pst.executeQuery();
            ArrayList<Part> returnArray = new ArrayList<Part>();
            while (rs.next()){
                returnArray.add(new Part(rs.getInt("id"),rs.getString("name"),rs.getString("test"),rs.getString("description"),rs.getInt("results")));

            }
            return returnArray;
        }
        catch (Exception ex){
            System.out.println("Exception : "+ex);
        }
        return null;
    }
}