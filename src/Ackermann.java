
public class Ackermann {

    public static int ackermannFunction(int m, int n) {
        if (m < 0 || n < 0) {
            throw new IllegalArgumentException("Non-negative args only!");
        }

        if (m == 0) {
            return n + 1;

        } else if (n == 0) {
            return ackermannFunction(m-1, 1);
        } else {
            // perforce (m > 0) && (n > 0)
            return ackermannFunction(m-1, ackermannFunction(m,n-1));
        }
    }

    public static void main(String[] args) {
        RunAM();
    }


    public static long RunAM(){
        long startTime = System.currentTimeMillis();

        int[] loads = {100,200,400,800,1600,3200,6400,12800,25600,51200};
        int n = 3;
        int m = 3;

        for (int j=0; j < loads.length; j++) {
            for (int i=0; i < loads[j]*10; i++) {
                ackermannFunction(m,n);
            }
            ProgramWindow.printToOutput("Test [" + j + "]   \n");

        }

        long endTime = System.currentTimeMillis();

        long sum = endTime - startTime;
        return sum;

    }

}
