
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;




class ProgramWindow extends JFrame {

  //Components for INTRO screen
  private JButton btnExit = new JButton("Exit");
  private JButton btnStart = new JButton("Start");
  private Image startPicture = ImageIO.read(new File("src/images/pic1.png"));
  private Image myPicture4 = ImageIO.read(new File("src/images/pic6.png"));
  private JLabel picLabel = new JLabel(new ImageIcon(startPicture));
  private ImageIcon imageIcon1 = new ImageIcon(myPicture4);

  //Components for SELECT screen
  private Image myPicture1 = ImageIO.read(new File("src/images/pic3.png"));
  private Image myPicture2 = ImageIO.read(new File("src/images/pic4.png"));
  private Image myPicture3 = ImageIO.read(new File("src/images/pic5.png"));


  //Components for SELECTED COMPONENT screen
  private String selectedWindow = "";
  private JButton btnSelProc = new JButton("PROCESSOR TESTS");
  private JButton btnSelGraph = new JButton("GRAPHICS CARD TESTS");
  private JButton btnSelMem = new JButton("MEMORY TESTS");
  private JLabel selLabel = new JLabel("Select Component type");


  //Common components
  private JButton btnBack = new JButton("Back");
  private JButton btnBegin = new JButton("Begin Test");
  private static JTextArea Output = new JTextArea("Select processor and process");
  private JScrollPane scrollOutput = new JScrollPane(Output,
          JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);


  //Components for PROCESSOR screen
  private String[] procTests = {"MonteCarlo","Ackermann", "Chrome Test"};
  private String[] processors = {"Processor"};
  private JComboBox procTestList = new JComboBox(procTests);
  private JComboBox procList = new JComboBox(processors);
  private JLabel processor = new JLabel("Selected Processor");
  private JLabel process = new JLabel("Selected Process");
  private JLabel outputName = new JLabel("Process output");
  private JLabel graphName = new JLabel("Graph Goes here !");



  //Components for GRAPHICS CARD screen
  private String[] graphTests = {"Test1","Test2","Test3"};
  private String[] graphicsCards = {"G-Card"};
  private JComboBox graphTestList = new JComboBox(graphTests);
  private JComboBox graphList = new JComboBox(graphicsCards);
  private JLabel gCard = new JLabel("Selected G card");

  //Components for MEMORY screen
  private String[] memTests = {"Test1","Test2","Test3"};
  private String[] memoryComponents = {"M-component 1"};
  private JComboBox memTestList = new JComboBox(memTests);
  private JComboBox memList = new JComboBox(memoryComponents);
  private JLabel memory = new JLabel("Selected M Component");



  //Sets up main window
  public ProgramWindow() throws IOException {
    setTitle("Artifact finder v0.02");
    setSize(500,600);
    setLocation(new Point(400,50));
    setLayout(null);
    setResizable(false);

    initMainMenu();
    initEvent();
  }

  //Sets up PROCESSOR screen
  private void initProcScreen(){
    selectedWindow = "Pr";
    btnExit.setBounds(900,535, 80,25);
    btnBegin.setVisible(true);
    btnBack.setVisible(true);
    procTestList.setVisible(true);
    procList.setVisible(true);
    Output.setVisible(true);
    scrollOutput.setVisible(true);
    processor.setVisible(true);
    process.setVisible(true);
    outputName.setVisible(true);
    graphName.setVisible(true);
    procTestList.setSelectedIndex(0);
    procList.setSelectedIndex(0);
  }

  //Sets up GRAPHICS CARD screen
  private void initGraphScreen(){
    selectedWindow = "Gr";
    btnExit.setBounds(900,535, 80,25);
    btnBack.setVisible(true);
    btnBegin.setVisible(true);
    graphTestList.setVisible(true);
    graphList.setVisible(true);
    Output.setVisible(true);
    scrollOutput.setVisible(true);
    gCard.setVisible(true);
    outputName.setVisible(true);
    graphName.setVisible(true);
    process.setVisible(true);
    graphTestList.setSelectedIndex(0);
    graphList.setSelectedIndex(0);

  }

  //Sets up MEMORY COMPONENT screen
  private void initMemScreen(){
    selectedWindow = "Mm";
    btnExit.setBounds(900,535, 80,25);
    btnBack.setVisible(true);
    btnBegin.setVisible(true);
    memTestList.setVisible(true);
    memList.setVisible(true);
    Output.setVisible(true);
    scrollOutput.setVisible(true);
    memory.setVisible(true);
    outputName.setVisible(true);
    graphName.setVisible(true);
    process.setVisible(true);
    memTestList.setSelectedIndex(0);
    memList.setSelectedIndex(0);

  }

  // sets scale and position of components
  private void initSelectScreenComponentScale(){
    myPicture1 = myPicture1.getScaledInstance(450,100, Image.SCALE_DEFAULT);
    myPicture2 = myPicture2.getScaledInstance(450,100, Image.SCALE_DEFAULT);
    myPicture3 = myPicture3.getScaledInstance(450,100, Image.SCALE_DEFAULT);

    selLabel.setBounds(100,20,300,50);
    btnSelProc.setBounds(20,200, 450,50);
    btnSelGraph.setBounds(20,300,450,50);
    btnSelMem.setBounds(20,400,450,50);

    btnBack.setBounds(810,535, 80,25);
    btnBegin.setBounds(680, 535, 120, 25);
    scrollOutput.setBounds(20,40,350,400);
    process.setBounds(20,535,150,25);
    outputName.setBounds(20,20,150,25);
    graphName.setBounds(400,20,150,25);

    procTestList.setBounds(150, 535, 150, 25);
    procList.setBounds(150, 500, 150, 25);
    processor.setBounds(20,500,150,25);

    graphTestList.setBounds(150, 535, 150, 25);
    graphList.setBounds(150, 500, 150, 25);
    gCard.setBounds(20,500,150,25);

    memTestList.setBounds(150, 535, 150, 25);
    memList.setBounds(150, 500, 150, 25);
    memory.setBounds(20,500,150,25);
  }


  // sets properties of components
  private void initSelectScreenComponentProperties(){
    btnSelProc.setIcon(new ImageIcon(myPicture1));
    btnSelGraph.setIcon(new ImageIcon(myPicture2));
    btnSelMem.setIcon(new ImageIcon(myPicture3));

    selLabel.setFont(new Font("Serif", Font.BOLD, 30));
    selLabel.setForeground(Color.WHITE);

    btnSelProc.setFont(new Font("Serif", Font.BOLD, 30));
    btnSelProc.setForeground(Color.BLACK);
    btnSelProc.setVerticalTextPosition(JButton.CENTER);
    btnSelProc.setHorizontalTextPosition(JButton.CENTER);
    process.setForeground(Color.WHITE);
    processor.setForeground(Color.WHITE);
    outputName.setForeground(Color.WHITE);
    graphName.setForeground(Color.WHITE);
    gCard.setForeground(Color.WHITE);
    memory.setForeground(Color.WHITE);

    btnSelGraph.setFont(new Font("Serif", Font.BOLD, 30));
    btnSelGraph.setForeground(Color.BLACK);
    btnSelGraph.setVerticalTextPosition(JButton.CENTER);
    btnSelGraph.setHorizontalTextPosition(JButton.CENTER);

    btnSelMem.setFont(new Font("Serif", Font.BOLD, 30));
    btnSelMem.setForeground(Color.BLACK);
    btnSelMem.setVerticalTextPosition(JButton.CENTER);
    btnSelMem.setHorizontalTextPosition(JButton.CENTER);
  }
  //Sets up SELECT screen
  private void initSelectScreen(){
    add(btnBack);
    add(btnSelProc);
    add(btnSelGraph);
    add(btnSelMem);
    add(selLabel);
    add(btnBegin);
    add(scrollOutput);
    add(outputName);
    add(process);
    add(graphName);


    add(procList);
    add(procTestList);
    add(processor);

    add(graphList);
    add(graphTestList);
    add(gCard);

    add(memList);
    add(memTestList);
    add(memory);
  }

  //Closes components from selected components screens
  private void CloseComponents(){
    CloseMemComponents();
    CloseProcComponents();
    CloseGraphComponents();
    btnBack.setVisible(false);
    btnBegin.setVisible(false);
    Output.setEditable(false);
    scrollOutput.setVisible(false);
  }

  //Sets up INTRO screen
  private void initMainMenu(){
    setContentPane(new JLabel(imageIcon1));
    btnExit.setBounds(400,535, 80,25);
    btnStart.setBounds(20,490, 450,25);
    Image myPictureS = startPicture.getScaledInstance(450,450, Image.SCALE_DEFAULT);
    picLabel = new JLabel(new ImageIcon(myPictureS));
    picLabel.setBounds(20,10,450,450);


    add(btnExit);
    add(btnStart);
    add(picLabel);
  }

  //Button events
  private void initEvent(){

    this.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e){
        System.exit(1);
      }
    });

    btnExit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        btnExitClick(e);
      }
    });

    btnStart.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        btnSelectClick(e);
      }
    });

    btnSelGraph.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        btnSelGraphClick(e);
      }
    });
    btnSelMem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        btnSelMemClick(e);
      }
    });
    btnSelProc.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        btnSelProcClick(e);
      }
    });
    btnBack.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        btnBackClick(e);
      }
    });
    btnBegin.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        btnBeginClick(e);
      }
    });

    scrollOutput.getVerticalScrollBar().addAdjustmentListener((AdjustmentEvent e) -> {
      Output.select(Output.getCaretPosition()
              * Output.getFont().getSize(), 0);
    });

  }

  //Button methods

  private void btnExitClick(ActionEvent evt){
    System.exit(0);
  }

//
  private void btnBeginClick(ActionEvent e) {
    clearOutput();
    switch (selectedWindow) {
      case "Pr": {
        switch ((String) procTestList.getSelectedItem()) {
          case "MonteCarlo": {
            Output.append("Selected MonteCarlo");
            MonteCarlo.RunMC();
            Output.append("Process complete");
            break;
          }
          case "Ackermann": {
            Output.append("Selected Ackermann");
            Ackermann.RunAM();
            Output.append("Process complete");
            break;
          }
          case "Eulers Solutions": {
            Output.append("Selected Eulers Solutions");
            p099 p1 = new p099();
            p1.run();
            Output.append("Process complete");
            break;
          }

          case "Chrome Test": {
            Output.append("Selected Chrome Test");
            Results p1 = new Results();
            p1.results();
            Output.append("Process complete");
            break;
          }
        }
      }
      case "Mr":
      {
        // Call processes for Memory components
      }
      case  "Gr":
      {
         // Call Processes for Grapgics card components
      }
    }
  }

  private void btnSelectClick(ActionEvent evt){
    picLabel.setVisible(false);
    btnStart.setVisible(false);
    initSelectScreenComponentScale();
    initSelectScreenComponentProperties();
    initSelectScreen();
    CloseComponents();
  }


  private void btnSelProcClick(ActionEvent evt){
    CloseSelectWindowComponents();
    setSize(1000,600);
    setLocation(new Point(150,50));
    initProcScreen();
  }

  private void btnSelGraphClick(ActionEvent evt){
    CloseSelectWindowComponents();
    setSize(1000,600);
    setLocation(new Point(150,50));
    initGraphScreen();
  }

  private void btnSelMemClick(ActionEvent evt){
    CloseSelectWindowComponents();
    setSize(1000,600);
    setLocation(new Point(150,50));
    initMemScreen();
  }

  private void btnBackClick(ActionEvent evt){
    btnBack.setVisible(false);
    btnBegin.setVisible(false);
    Output.setVisible(false);
    scrollOutput.setVisible(false);
    process.setVisible(false);
    outputName.setVisible(false);
    graphName.setVisible(false);
    clearOutput();
    switch (selectedWindow) {
      case "Pr": {
        CloseProcComponents();
      }
      case "Gr": {
        CloseGraphComponents();
      }
      case "Mm": {
        CloseMemComponents();
      }
    }
    OpenSelectWindowComponents();
    btnExit.setBounds(400,535, 80,25);
    setSize(500,600);
    setLocation(new Point(400,50));
  }

  //Screen change methods
  private void CloseProcComponents(){
    procTestList.setVisible(false);
    procList.setVisible(false);
    process.setVisible(false);
    processor.setVisible(false);
    outputName.setVisible(false);
    graphName.setVisible(false);
  }
  private void CloseGraphComponents(){
    graphTestList.setVisible(false);
    graphList.setVisible(false);
    gCard.setVisible(false);
  }
  private void CloseMemComponents(){
    memTestList.setVisible(false);
    memList.setVisible(false);
    memory.setVisible(false);
  }

  private void CloseSelectWindowComponents(){
    btnSelProc.setVisible(false);
    btnSelGraph.setVisible(false);
    btnSelMem.setVisible(false);
    selLabel.setVisible(false);
  }

  private void OpenSelectWindowComponents(){
    btnSelProc.setVisible(true);
    btnSelGraph.setVisible(true);
    btnSelMem.setVisible(true);
    selLabel.setVisible(true);
  }

  public static void printToOutput(String line) {
    Output.append(line);
    Output.update(Output.getGraphics());

  }

  void clearOutput(){
    Output.setText(null);
  }

}

