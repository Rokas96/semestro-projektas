
public class Part {
    private int id;
    private String name;
    private String nameOfTheTest;
    private String description;
    private int testResult;
    public Part(){}
    public Part(int id, String name, String nameOfTheTest, String description, int testResult){
        this.id=id;
        this.name=name;
        this.nameOfTheTest=nameOfTheTest;
        this.description=description;
        this.testResult=testResult;
    }
    public int getID(){
        return id;
    }
    public String getName(){
        return name;
    }
    public String getNameOfTheTest(){
        return nameOfTheTest;
    }
    public String getDescription(){
        return description;
    }
    public int getTestResult(){
        return testResult;
    }
}
