import java.util.Scanner;
public class MonteCarlo {


    //"throwing" a dart
    public static boolean[] dartThrow(int r, int d) {
        boolean[] booleanArray = new boolean[d];
        for (int i = 0; i < d; i++) {
            double xCoord = Math.random() * r;
            double yCoord = Math.random() * r;
            if (((Math.pow(xCoord,2) + Math.pow(yCoord,2)) <= r*r)) {
                booleanArray[i] = true;
            } else {
                booleanArray[i] = false;
            }
        }
        return booleanArray;
    }

    //calculating pi from throwing results
    public static double piEstimater(boolean[] h, int d) {
        int trueCounter = 0;
        for (int i = 0; i < h.length; i++) {
            if (h[i] == true) {
                trueCounter++;
            }
        }
        return 4 * ((double) trueCounter / d);
    }

    //printing results
    public static void printer(double[] a) {
        System.out.println("     Pi Estimation Tool    ");
        System.out.println("---------------------------");
        for (int i = 0; i < a.length; i++) {
            System.out.print("Trial [" + i + "]: pi = ");
            System.out.printf("%6f\n", a[i]);
        }
    }

    public static void main(String[] args) {
        RunMC();
    }

    public static long RunMC(){
        long startTime = System.currentTimeMillis();
        ProgramWindow.printToOutput("     Pi Estimation Tool    \n");
        ProgramWindow.printToOutput("---------------------------\n");
        //variables
        Scanner in = new Scanner(System.in);
//        int radius = 100;
 //       int darts = 10000;
//        int trials= 1000;
        int[] loads = {100,200,400,800,1600,3200,6400,12800};
        for (int j=0; j < loads.length; j++) {
            double[] arrayOfEstimates = new double[loads[j]];
            int i = 0;
            for (double a : arrayOfEstimates) {
                boolean[] hitCounter = dartThrow(loads[j], loads[j]);
                double piEstimate = piEstimater(hitCounter, loads[j]);
                arrayOfEstimates[i] = piEstimate;
                i++;
            }
            printForGUI(arrayOfEstimates,j);




            //  System.out.println("Enter the number of darts to calculate for: ");
            //  darts = in.nextInt();
            //   System.out.println("Enter the number of trials to calculate for: ");
            //  trials = in.nextInt();
            //cia darei ka ant virsaus karolis dare?  nu ka zn..tsg dadejau po Time() metoda visoms s

        }
        long endTime = System.currentTimeMillis();

        //System.out.println("That took " + (endTime - startTime) + " milliseconds");
        long sum;
        return  sum = endTime - startTime;


    }

    public static void printForGUI(double[] a,int loadNr){
        ProgramWindow.printToOutput("Test [" + loadNr + "] \n");
        for (int i = 0; i < a.length; i++) {
            //ProgramWindow.printToOutput("Trial [" + i + "]: pi = ");
           // ProgramWindow.printToOutput(String.valueOf(a[i])+ "\n");
        }

    }
}

